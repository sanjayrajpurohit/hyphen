var User = require("../app/models/user.model");

module.exports = {
  
  create: data => {
    var user = new User(data);
    return user.save();
  },

  find: email => {
    return User.find({ email: email });
  }

};
