const UserRepository = require("../../repositories/UserRepository");
const UserSessionRepository = require("../../repositories/UserSessionRepository");
const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');
module.exports = {

  create: function(req, res) {
    const { body } = req;
    const {
      firstname,
      lastname,
      username,
      password,
      email
    } = body;
    if(!firstname) {
      res.send({
        success: false,
        message: 'Error: firstname cannot be empty',
        status: 500
      });
    }
    if(!lastname) {
      res.send({
        success: false,
        message: 'Error: lastname cannot be empty',
        status: 500
      });
    }
    if(!username) {
      res.send({
        success: false,
        message: 'Error: username cannot be empty',
        status: 500
      });
    }
    if(!password) {
      res.send({
        success: false,
        message: 'Error: password cannot be empty',
        status: 500
      });
    }
    if(!email) {
      res.send({
        success: false,
        message: 'Error: email cannot be empty',
        status: 500
      });
    }
    body.email = email.toLowerCase(); 
    body.password =  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    UserRepository.find(body.email)
    .then(function (prevusers) {  
        if(prevusers.length > 0) {
          res.send({
            success: false,
            message: 'Account already exists',
          })  
        }
        else {
          UserRepository.create(body)
            .then(result => {
              res.send({
                success: true,
                _id: result._id
              });
            })
            .catch(message => {
              res.send({
                success: false,
                message: 'error occured'
              });
            });
        }
    })  
  },

  get: function(req, res) {
    const { body } = req;
    const {
      email,
      password
    } = body;
    body.email = email.toLowerCase();
    // console.log(email);
    // console.log(password);
    if(!password) {
      res.send({
        success: false,
        message: 'Error: password cannot be empty',
        status: 500
      });
    }
    if(!email) {
      res.send({
        success: false,
        message: 'Error: email cannot be empty',
        status: 500
      });
    }  
    UserRepository.find(email)
      .then(function (data) {
        user = data[0];
        if(user && bcrypt.compareSync(password, user.password)) {
          UserSessionRepository.create(user._id)
          .then(function(doc) {
            if(doc) {
              res.status(200).json({ 
                success: true,
                message: 'Welcome Mr.' + user.firstname + '!!',
                token: doc._id
              });				
            }
          });
        }	
        else {
          res.status(404).json({ message: 'Wrong credentials!!'});	
        }	
      })
      .catch(message => {
        res.send({
          success: false,
          message
        });
      });
  },

  verify: function(req, res) {
    const token = req.params.id;
    // console.log(token);
    UserSessionRepository.find(token)
    .then(function(doc) {
      if(doc.length != 0) {
        res.status(200).json({
          success: true,
          message: 'token found!! Valid signin',
          doc
        })
      }
      else {
        res.status(500).json({
          success: false,
          message: 'Please Login!',
        })
      }
    });
  },

  logout: function(req, res) {
    var token = req.query.token;
    // console.log(token);
    UserSessionRepository.update(token, (doc) => {
      console.log('it is returned');
      // console.log(doc);
    });
  }

};
