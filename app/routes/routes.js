module.exports = function(app, db) {
  const USER = require("../controllers/user.controller.js");
  app.post("/account/signup", USER.create);
  app.post("/account/signin", USER.get);
  app.get("/account/verify/:id",USER.verify);
  app.get("/account/logout/",USER.logout);
};
