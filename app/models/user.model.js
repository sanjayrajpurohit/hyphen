var mongoose = require("./mongoose");
var bcrypt = require("bcrypt");

const UserSchema = new mongoose.Schema({
  firstname: { 
    type: String,
    default: ''
  },
  lastname: {
    type: String,
    default: ''
  },
  username: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    default: ''
  },
  password: {
    type: String,
    default: ''
  },
  isdeleted: {
    type: Boolean,
    default: false
  }
  
});

module.exports = mongoose.model("USER", UserSchema);
