var mongoose = require("./mongoose");

const UserSessionSchema = new mongoose.Schema( {
    userId: {
        type: String,
        default: ''
    },
    isdeleted: {
        type: Boolean,
        default: false
    },

});

module.exports = mongoose.model('UserSession', UserSessionSchema);